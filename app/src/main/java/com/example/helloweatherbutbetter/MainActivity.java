package com.example.helloweatherbutbetter;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;

    private EditText editTextTown;
    private TextView textNowDate, textNowWeatherTemp, textNowWeatherType, textNowWeatherLikeTemp;
    private Button editTownConfirm;
    private ImageView imageWeatherType;

    private Date nowDate;

    private View weatherLayout;
    private View forecastLayout;
    private RecyclerView forecastLinearContainer;

    private View writeTownText;
    private TextView textError;
    private View progressBarLayout;

    private WeatherServiceAPI weatherAPI;
    private WeatherInfo weatherInfo;
    private ArrayList<WeatherInfo> weatherInfoList;
    private boolean requestIsRunning = false;

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // save weather data in ViewModel
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.getWeather().observe(this, weather -> {
            weatherInfo = weather;
        });
        viewModel.getWeatherForecast().observe(this, forecast -> {
            weatherInfoList = forecast;
            if (weatherInfoList != null) updateViewsWeather();
        });

        sharedPref = getSharedPreferences(
                getString(R.string.preference_file_name), MODE_PRIVATE);

        weatherLayout = findViewById(R.id.blockWeatherNowDate);
        forecastLayout = findViewById(R.id.blockWeatherForecast);
        writeTownText = findViewById(R.id.textWriteTown);
        writeTownText.setVisibility(View.GONE);
        textError = findViewById(R.id.textError);
        textError.setVisibility(View.GONE);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        progressBarLayout.setVisibility(View.GONE);

        editTextTown = findViewById(R.id.editTown);
        editTownConfirm = findViewById(R.id.editTownConfirm);

        textNowDate = findViewById(R.id.textNowDate);
        nowDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        textNowDate.setText("Today, " + formatter.format(nowDate));

        textNowWeatherTemp = findViewById(R.id.textWeatherTemp);
        textNowWeatherLikeTemp = findViewById(R.id.textNowWeatherLikeTemp);
        textNowWeatherType = findViewById(R.id.textWeatherType);
        textNowWeatherType.setText("select town");

        forecastLinearContainer = findViewById(R.id.forecastLinearContainer);

        imageWeatherType = findViewById(R.id.imageWeatherType);

        createWeatherAPI();

        editTownConfirm.setOnClickListener(view -> {
            if (!requestIsRunning) {
                getWeather();
            }
        });

        String townName = sharedPref.getString(getString(R.string.data_town_name), null);
        if (townName != null) {
            editTextTown.setText(townName);
            if (weatherInfoList == null)
                editTownConfirm.callOnClick();
            else updateViewsWeather();

        } else {
            writeTownText.setVisibility(View.VISIBLE);
            progressBarLayout.setVisibility(View.GONE);
            weatherLayout.setVisibility(View.GONE);
            forecastLayout.setVisibility(View.GONE);
        }
    }

    private void createWeatherAPI() {
        final String url = "https://api.openweathermap.org/";

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        weatherAPI = retrofit.create(WeatherServiceAPI.class);
    }

    private void getWeather() {
        requestIsRunning = true;
        weatherLayout.setVisibility(View.GONE);
        forecastLayout.setVisibility(View.GONE);
        progressBarLayout.setVisibility(View.VISIBLE);
        weatherAPI.getWeather(editTextTown.getText().toString(), "metric").enqueue(new WeatherResponse());
    }

    private void updateViewsWeather() {
        if (weatherInfoList == null || weatherInfoList.size() == 0) {
            progressBarLayout.setVisibility(View.GONE);
            textError.setVisibility(View.VISIBLE);
        } else {
            RecycleViewAdapterForecast adapter = new RecycleViewAdapterForecast(weatherInfoList);
            forecastLinearContainer.setAdapter(adapter);
            weatherLayout.setVisibility(View.VISIBLE);
            forecastLayout.setVisibility(View.VISIBLE);
            progressBarLayout.setVisibility(View.GONE);
            writeTownText.setVisibility(View.GONE);
            textError.setVisibility(View.GONE);

            textNowWeatherType.setText(weatherInfo.description);
            textNowWeatherTemp.setText(weatherInfo.temp);
            textNowWeatherLikeTemp.setText(weatherInfo.feelLike);

            switch (weatherInfo.type) {
                case "Thunderstorm":
                    imageWeatherType.setImageResource(R.drawable.weather_storm);
                    break;
                case "Drizzle":
                    imageWeatherType.setImageResource(R.drawable.weather_rain);
                    break;
                case "Rain":
                    imageWeatherType.setImageResource(R.drawable.weather_hard_rain);
                    break;
                case "Snow":
                    imageWeatherType.setImageResource(R.drawable.weather_snow);
                    break;
                case "Atmosphere":
                    imageWeatherType.setImageResource(R.drawable.weather_few_clouds);
                    break;
                case "Clear":
                    imageWeatherType.setImageResource(R.drawable.weather_clear_sky);
                    break;
                case "Clouds":
                    imageWeatherType.setImageResource(R.drawable.weather_scattered_clouds);
                    break;
            }
        }

    }

    private class WeatherResponse implements Callback<JsonObject> {
        private JSONObject jsonWeatherNow, jsonWeatherForecast;

        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            if (response.isSuccessful()) {
                try {
                    jsonWeatherNow = new JSONObject(response.body().toString());

                    double lon, lat;
                    lon = jsonWeatherNow.getJSONObject("coord").getDouble("lon");
                    lat = jsonWeatherNow.getJSONObject("coord").getDouble("lat");

                    weatherAPI.getForecast(Double.toString(lat), Double.toString(lon), "metric").enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.isSuccessful()) {
                                try {
                                    jsonWeatherForecast = new JSONObject(response.body().toString());

                                    weatherInfo.type = jsonWeatherNow.getJSONArray("weather").getJSONObject(0).getString("main");
                                    weatherInfo.description = jsonWeatherNow.getJSONArray("weather").getJSONObject(0).getString("description");

                                    int temp = Math.round(Float.parseFloat(jsonWeatherNow.getJSONObject("main").getString("temp")));
                                    if (temp >= 0) {
                                        weatherInfo.temp = "+" + temp;
                                    } else {
                                        weatherInfo.temp = "-" + temp;
                                    }
                                    temp = Math.round(Float.parseFloat(jsonWeatherNow.getJSONObject("main").getString("feels_like")));
                                    if (temp >= 0) {
                                        weatherInfo.feelLike = "+" + temp;
                                    } else {
                                        weatherInfo.feelLike = "-" + temp;
                                    }
                                    viewModel.saveWeather(weatherInfo);

                                    JSONArray weatherDaysList;

                                    SimpleDateFormat formatToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    SimpleDateFormat formatToString = new SimpleDateFormat("dd:MM HH:mm");

                                    weatherDaysList = jsonWeatherForecast.getJSONArray("list");
                                    for (int i = 0; i < weatherDaysList.length(); i++) {
                                        JSONObject jsonDayObject = weatherDaysList.getJSONObject(i);
                                        WeatherInfo weatherDayInfo = new WeatherInfo();

                                        Date date = formatToDate.parse(jsonDayObject.getString("dt_txt"));
                                        weatherDayInfo.date = formatToString.format(date);

                                        temp = Math.round(Float.parseFloat(jsonDayObject.getJSONObject("main").getString("temp")));
                                        if (temp >= 0) {
                                            weatherDayInfo.temp = "+" + temp;
                                        } else {
                                            weatherDayInfo.temp = "-" + temp;
                                        }
                                        temp = Math.round(Float.parseFloat(jsonDayObject.getJSONObject("main").getString("feels_like")));
                                        if (temp >= 0) {
                                            weatherDayInfo.feelLike = "+" + temp;
                                        } else {
                                            weatherDayInfo.feelLike = "-" + temp;
                                        }

                                        weatherDayInfo.type = jsonDayObject.getJSONArray("weather").getJSONObject(0).getString("main");
                                        weatherDayInfo.description = jsonDayObject.getJSONArray("weather").getJSONObject(0).getString("description");

                                        weatherInfoList.add(weatherDayInfo);
                                    }
                                    viewModel.saveForecast(weatherInfoList);
                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString(getString(R.string.data_town_name), editTextTown.getText().toString());
                                    editor.apply();
                                } catch (JSONException | ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                System.out.println(response.errorBody());
                            }

                            requestIsRunning = false;
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            progressBarLayout.setVisibility(View.GONE);
                            textError.setVisibility(View.VISIBLE);
                            requestIsRunning = false;
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                weatherInfoList.clear();
                viewModel.saveForecast(weatherInfoList);
                requestIsRunning = false;
            }
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            progressBarLayout.setVisibility(View.GONE);
            textError.setVisibility(View.VISIBLE);
            requestIsRunning = false;
            updateViewsWeather();
        }

    }
}
