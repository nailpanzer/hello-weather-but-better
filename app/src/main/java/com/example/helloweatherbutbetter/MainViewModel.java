package com.example.helloweatherbutbetter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class MainViewModel extends ViewModel {

    private MutableLiveData<WeatherInfo> weatherNow;
    private MutableLiveData<ArrayList<WeatherInfo>> weatherForecast;

    public LiveData<WeatherInfo> getWeather() {
        if (weatherNow == null) {
            weatherNow = new MutableLiveData<>();
            weatherNow.setValue(new WeatherInfo());
        }
        return weatherNow;
    }

    public void saveWeather(WeatherInfo weatherInfo) {
        weatherNow.setValue(weatherInfo);
    }

    public LiveData<ArrayList<WeatherInfo>> getWeatherForecast() {
        if (weatherForecast == null) {
            weatherForecast = new MutableLiveData<>();
            weatherForecast.setValue(new ArrayList<>());
        }
        return weatherForecast;
    }

    public void saveForecast(ArrayList<WeatherInfo> forecastList){
        weatherForecast.setValue(forecastList);
    }

    @Override
    protected void onCleared() {

    }

}
