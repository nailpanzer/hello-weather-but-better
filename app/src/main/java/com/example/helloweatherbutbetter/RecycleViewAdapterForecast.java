package com.example.helloweatherbutbetter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecycleViewAdapterForecast extends RecyclerView.Adapter<RecycleViewAdapterForecast.ForecastViewHolder> {

    public class ForecastViewHolder extends RecyclerView.ViewHolder {

        private WeatherInfo weatherInfo;

        private ImageView imageView;
        private TextView textDate, textTemp, textLikeTemp, tempBarCount;
        private View graphLayout, tempBarGraph;

        public ForecastViewHolder(View view) {
            super(view);

            textDate = view.findViewById(R.id.textDayForecastDate);
            textTemp = view.findViewById(R.id.textDayTemp);
            textLikeTemp = view.findViewById(R.id.textDayLikeTemp);

            imageView = view.findViewById(R.id.imageView);

            graphLayout = view.findViewById(R.id.graphLayout);
            tempBarCount = view.findViewById(R.id.tempBarCount);
            tempBarGraph = view.findViewById(R.id.tempBarGraph);
        }

        public void setWeatherInfo(WeatherInfo newWeatherInfo) {
            weatherInfo = newWeatherInfo;

            textDate.setText(weatherInfo.date);
            textTemp.setText(weatherInfo.temp);
            textLikeTemp.setText(weatherInfo.feelLike);
            tempBarCount.setText(weatherInfo.temp);

            // предположим, что температура заключена в промежутке -30; 30
            ViewGroup.LayoutParams params = tempBarGraph.getLayoutParams();
            params.height = graphLayout.getLayoutParams().height * (Integer.parseInt(weatherInfo.temp) + 30) / 60;
            tempBarGraph.setLayoutParams(params);

            switch (weatherInfo.type) {
                case "Thunderstorm":
                    imageView.setImageResource(R.drawable.weather_storm);
                    break;
                case "Drizzle":
                    imageView.setImageResource(R.drawable.weather_rain);
                    break;
                case "Rain":
                    imageView.setImageResource(R.drawable.weather_hard_rain);
                    break;
                case "Snow":
                    imageView.setImageResource(R.drawable.weather_snow);
                    break;
                case "Atmosphere":
                    imageView.setImageResource(R.drawable.weather_few_clouds);
                    break;
                case "Clear":
                    imageView.setImageResource(R.drawable.weather_clear_sky);
                    break;
                case "Clouds":
                    imageView.setImageResource(R.drawable.weather_scattered_clouds);
                    break;
            }

        }

    }

    private List<WeatherInfo> weatherInfoArray;

    public RecycleViewAdapterForecast(List<WeatherInfo> dataSet) {
        weatherInfoArray = dataSet;
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_day_forecast, viewGroup, false);

        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder viewHolder, final int position) {
        viewHolder.setWeatherInfo(weatherInfoArray.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherInfoArray.size();
    }
}
