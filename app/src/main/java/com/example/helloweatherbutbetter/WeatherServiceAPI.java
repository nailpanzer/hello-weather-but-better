package com.example.helloweatherbutbetter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherServiceAPI {

    @GET("data/2.5/weather?appid=5dc2c80609fb9c36d415e2b069d55a50&")
    Call<JsonObject> getWeather(
            @Query("q") String town,
            @Query("units") String units
    );

    @GET("data/2.5/forecast?appid=5dc2c80609fb9c36d415e2b069d55a50&")
    Call<JsonObject> getForecast(
            @Query("lat") String lat,
            @Query("lon") String lon,
            @Query("units") String units
    );

}
