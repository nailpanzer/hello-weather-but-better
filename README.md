# Hello Weather But Better

## Load

После того как пользователь выбрал город появляется анимация загрузки:

![plot](./screenshots/load.jpg)

## Сurrent weather

После загрузки можно увидеть текущую погоду:

![plot](./screenshots/nowweather.jpg)

## Weather forecast

Также отображается прогноз погоды на близжайшие дни:

![plot](./screenshots/forecast1.jpg)
